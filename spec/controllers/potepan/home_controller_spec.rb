require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "#index" do
    let!(:products) { create_list(:product, 11) }

    before do
      get :index
    end

    it "response successful" do
      expect(response).to be_successful
    end

    it "render index template" do
      expect(response).to render_template :index
    end

    it 'assigns @products' do
      expect(assigns(:products)).to eq products
    end

    it 'new_products.size is 10' do
      expect(assigns(:new_products).size).to eq 10
    end
  end
end
