require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:other_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "response successful" do
      expect(response).to be_successful
    end

    it "render show template" do
      expect(response).to render_template :show
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'assigns @taxonomies' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it 'to include product' do
      expect(assigns(:products)).to include product
    end

    it 'not_to include other_product' do
      expect(assigns(:products)).not_to include other_product
    end
  end
end
