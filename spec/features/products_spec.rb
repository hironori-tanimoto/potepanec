require 'rails_helper'

RSpec.feature 'products', type: :feature do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario 'visit show page' do
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    within '.productsContent' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end
end
