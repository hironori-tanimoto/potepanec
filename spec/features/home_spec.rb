require 'rails_helper'

RSpec.feature 'home', type: :feature do
  let!(:product) { create(:product) }
  let!(:new_product) { create(:product) }

  background do
    visit potepan_path
  end

  scenario 'visit index page' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario 'click new_product.name and redirect to potepan_product_path' do
    click_on new_product.name
    expect(current_path).to eq potepan_product_path(new_product.id)
  end
end
