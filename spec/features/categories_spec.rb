require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'visit show page' do
    expect(page).to have_title "#{taxon.name} | Potepanec"
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end
    within '.productCaption' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  scenario 'click product.name and redirect to product_path' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
