class Potepan::HomeController < ApplicationController
  MAX_DISPRAY_NUMBER_OF_NEW_PRODUCTS = 10

  def index
    @products = Spree::Product.all.includes(master: [:default_price, :images])
    @new_products = @products.order(available_on: "DESC").limit(MAX_DISPRAY_NUMBER_OF_NEW_PRODUCTS)
  end
end
